# LiveScript Sublime Text 2 package.

## Inspiration

This is greatly inspired by [@surjikal][surjikal]'s [Coffee Compile][coffee-compile]
and [my own][joneshf] [Roy Complie][roy-compile]

## Usage

Compile the entire file by not selecting any text (or all the text).
Compile a section by selecting just that section.
Keyboard shortcut `Alt-Shift-L`
Command Pallette integration `Live Script`
Context menu `right click`

## Install

### Package Control

Coming soon.

### Manual

Clone this repository from your Sublime packages directory:

#### Linux

```
$ cd ~/.config/sublime-text-2/Packages
$ git clone https://github.com/joneshf/sublime-livescript
```

#### Macosx (untested)

```
$ cd "~/Library/Application Support/Sublime Text 2/Packages"
$ git clone https://github.com/joneshf/sublime-livescript
```

#### Windows (untested)

```
$ cd "%APPDATA%\Sublime Text 2"
$ git clone https://github.com/joneshf/sublime-livescript
```

[surjikal]: https://github.com/surjikal
[coffee-compile]: https://github.com/surjikal/sublime-coffee-compile
[joneshf]: https://github.com/joneshf
[roy-compile]: https://github.com/joneshf/RoyCompile
